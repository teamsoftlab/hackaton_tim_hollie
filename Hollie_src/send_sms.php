<?php


if (!isset($_SERVER['PHP_AUTH_USER'])) {
        header('WWW-Authenticate: Basic realm="My Realm"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Text to send if user hits Cancel button';
        exit;
} else if ($_SERVER['PHP_AUTH_USER']!="hollie" || $_SERVER['PHP_AUTH_PW']!="holliehackathon") {
        header('HTTP/1.0 401 Unauthorized');
        echo 'Authentication failed';
        exit;
}

//Api key da usare per invocare il servizio SMS
$APIKEY="<INSERIRE_LA_API_KEY>";

// Testo del sms, a regime dovrà essere dinamico
$MESSAGE="Il codice promozionale è: HOLLIEPRO";

// log chiamata del servizio
file_put_contents("sms.log", date("Y-m-d H:i:s")." >> INVOCATO SERVIZIO\n", FILE_APPEND|LOCK_EX);

// recupero del json inviato da DialogFlow
$data = json_decode(file_get_contents('php://input'), true);

// recupero del numero di telefono dal json
$telefono= $data["queryResult"]["parameters"]["phone-number"];

// log del numero di telefono recuperato
file_put_contents("sms.log", date("Y-m-d h:i:s")." >> TELEFONO RICEVUTO: ".$telefono."\n", FILE_APPEND|LOCK_EX);


// chiamata alla api invio sms
$curl = curl_init();

curl_setopt_array($curl, array(
          CURLOPT_URL => "https://hackathon.tim.it/sms/mt",
            CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS =>"{\n    \"address\": \"tel:+39$telefono\",\n    \"message\": \"$MESSAGE\"\n}",
                            CURLOPT_HTTPHEADER => array(
                                        "apikey: $APIKEY",
                                            "Content-Type: application/json"
                                              ),
                                      ));

$response = curl_exec($curl);

curl_close($curl);


// invio response
echo '{"fulfillmentMessages": {"text": {"text": ["Text response from webhook"]}}]}';
