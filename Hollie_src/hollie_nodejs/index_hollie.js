'use strict'

const record = require('./microphone');
const AudioContext = require('web-audio-api').AudioContext;
const Speaker = require('speaker');

const player = require('audio-play');
const load = require('audio-loader');
const through2 = require('through2');
const uuid = require('uuid/v1');
// Imports the Dialogflow library
const dialogflow = require('dialogflow').v2;

const projectId = 'newagent-rmwkwp';
const sessionId = uuid();
const encoding = 'AUDIO_ENCODING_LINEAR_16' ;
const sampleRateHertz = 16000;
const languageCode = 'it-IT';

const fs = require('fs');
const util = require('util');
//Create a websocket
const ws = require('ws');

const wss = new ws.Server({ port: 8080 });

//client connected to the websocket
var client = null;

//var exit = false;
var audioPlayer;
wss.on('connection', function(ws, req) {

    console.log('New client connected.  ', req.headers['sec-websocket-key']);

    client = ws;
    //clients[req.headers['sec-websocket-key']] = ws;
    
    send('{"filename":"albero_rotante.mp4"}');

});


function send(message) {
    //const message = '{"intent":"Prova","messaggio":"Messaggio"}'

    //for (var key in clients) {
    //    clients[key].send(message);
    //}
    if (client !== null)
        client.send(message);

    console.log('Send message ', message);

}

//open the serial port
var Serialport = require("serialport"); 
const Readline = require('@serialport/parser-readline')
var port = new Serialport("COM4", {
  baudRate: 115200
});
const parser = new Readline()
port.pipe(parser);



var isRunning = false;
var manualEnd;
//var audioPlayer;
//wait for serial port data
parser.on('data', async command => {
    if (command.trim() === 'ON') {
        if (!isRunning){
            audioPlayer = null;
            manualEnd=false;
            isRunning = true;
            console.log('> ', command.trim() ===  'ON');
            send('{"filename":"albero_parlante.mp4"}');
            load('./welcome.wav').then(player);
            //wait for reproduce the welcome utterance
            await new Promise((res,reject)=>{
                setTimeout(()=>{
                    res();
                },4500);
            });
            send('{"filename":"albero_ciglio.mp4"}');
            run();
        }else {
            console.log('Stop conversation')
            isRunning = false;
            manualEnd = true;
            if (audioPlayer != null)
                audioPlayer.pause();
            send('{"filename":"albero_rotante.mp4"}')
        }
    }
});

/*

var isRunning = false;

process.stdin.once('data', async data =>{
    if (!isRunning ) {
        isRunning = true;
        send('{"filename":"albero_parlante.mp4"}');
        load('./welcome.wav').then(player);
        //wait for reproduce the welcome utterance
        await new Promise((res,reject)=>{
            setTimeout(()=>{
                res();
            },6500);
        });
        send('{"filename":"albero_ciglio.mp4"}');
        run();
    }
});

*/
//option for recording (microphone)
const opts= {
    rate: '16000', 
    endian: 'big',
    channels: '1',
    encoding: 'signed-integer',
    debug: true,
    exitOnSilence: 15
}

// DIALOGFLOW - Instantiates a session client
const sessionClient = new dialogflow.SessionsClient({projectid: projectId,keyFilename: './newagent-rmwkwp-b66466e4c976.json'});
  
const sessionPath = sessionClient.sessionPath(projectId, sessionId);

//options for DIALOGFLOW request
const initialStreamRequest = {
    session: sessionPath,
    queryInput: {
        //text:{
        //    text: 'prova',
        //    languageCode: languageCode
        //}
        audioConfig: {
            audioEncoding: encoding,
            sampleRateHertz: sampleRateHertz,
            languageCode: languageCode,
            //singleUtterance: true
        }
    },
    outputAudioConfig: {
        audioEncoding: 'OUTPUT_AUDIO_ENCODING_LINEAR_16',
        sampleRateHertz: 44100, 
        synthesizeSpeechConfig: {
            speakingRate: 1.0, 
            pitch: 0.0, 
            volumeGainDb: 0.0,
            effectsProfileId: ['large-home-entertainment-class-device']	,
            voice: {
                name: "it-IT-Standard-B",
                ssmlGender: 'SSML_VOICE_GENDER_FEMALE'
              }
        }
    },
};
  
//class for handle voice interaction
class GoogleSpeech {

    constructor(request){
        this.request = request;
        this.stream = sessionClient.streamingDetectIntent();
        this.result = '';
        this.unpipeTimeout = null;
        this.listenFor = 60000;
        this.stream.write(this.request);
        this.mic = record(opts);
    }

    startStream(firstIteration){
        return new Promise(
            (Resolve,Reject)=>{

                var self = this;

                this.stream.on('pipe', function(){
                    console.log('PIPING > GOOGLE');
                    self.unpipeTimeout = setTimeout(()=>{
                        console.log('UNPIPE GOOGLE FOR TIMEOUT');
                        self.unpipeTimeout= null;
                        tstream.unpipe(self.stream);
                        self.mic.getAudioStream().unpipe(tstream);
                        self.mic.stop();
                    },self.listenFor);
                    
                })

                this.stream.on('error', function(err){
                    console.error('Errore GOOGLE', err);
                })

                this.stream.on('close', function(){
                    console.log('GOOGLE PIPE CLOSED');
                })

                this.stream.on('data', async data => {
                    if (data.recognitionResult) {
                            console.log(
                                'Intermediate transcript: ',data.recognitionResult.transcript);
                        }
                        if (data.queryResult != null){
                            
                            console.log('Trascript finito', data.queryResult.intent);
                            if (data.queryResult.intent !== null )
                                self.result = data.queryResult;
                            else self.result= null;
                            
                            if (data.queryResult.fulfillmentMessages.length == 0)
                                Resolve('end_conversation');
                        }

                        //wait for audio response
                        if (data.outputAudio.length>0){
                            if  (firstIteration){
                                send('{"filename":"albero_vibrante_pallina_cadente.mp4"}');

                                //wait for reproduce the welcome utterance
                                await new Promise((res,reject)=>{
                                    setTimeout(()=>{
                                        res();
                                    },4500);
                                });
                            }
                            const outputAudio = data.outputAudio;
                            //util.promisify(fs.writeFile)('welcome.wav', outputAudio, 'binary');
                            const context = new AudioContext();
                            //create a speaker output stream
                            context.outStream = new Speaker({
                                channels: context.format.numberOfChannels, 
                                bitDepth: context.format.bitDepth, 
                                sampleRate: context.sampleRate
                            });
                            //convert the audio buffer
                            var arrayBuffer = new ArrayBuffer(outputAudio.length);
                            var bufferView = new Uint8Array(arrayBuffer);
                            for (let i = 0; i < outputAudio.length; i++) {
                                bufferView[i] = outputAudio[i];
                            }
                            //decode audio data for  reproduce
                            context.decodeAudioData(
                                arrayBuffer,
                                function(buffer) {
                                    const options={
                                        start: 0,
                                        end: buffer.duration,
                                        loop: false,
                                        rate: 1,
                                        detune: 0,
                                        volume: 1 ,
                                        autoplay: false
                                    }

                                    if (self.result.intent.displayName.indexOf('- Map') !== -1 )
                                        send('{"filename":"albero_mappa.mp4"}');
                                    else send('{"filename":"albero_pallina_parlante.mp4"}');
                                    
                                    //play audio response 
                                    audioPlayer = player(buffer,options, function(){
                                        //wait for all duration of the response
                                        setTimeout(()=>{
                                            //check if the response is the end of conversation
                                            if (self.result.diagnosticInfo != null && self.result.diagnosticInfo.fields.end_conversation.boolValue)
                                            {
                                                Resolve('end_conversation');
                                            }
                                            else {
                                                console.log('Finito di chiacchierare, posso riaccendere il microfono');
                                                Resolve('continue');
                                            }
                                        }, Math.floor(2000));
                                    });
                                    audioPlayer.play();
                                }, 
                                function(err){
                                    console.log(err);
                                });
                        }    
                        else {
                            console.log('Empty utterance')
                           // Resolve('end_conversation');
                        }
                        
                });
            
                this.stream.on('unpipe', function(src){
                    console.log('UNPIPE > GOOGLE');
                    tstream.end();
                    self.stream.end();
                })

                this.stream.on('finish', function(){
                    console.log('FINISHED > GOOGLE');
                    if (self.unpipeTimeout != null){
                        clearTimeout(self.unpipeTimeout);
                        self.unpipeTimeout = null;
                        console.log('CLEARING TIMEOUT > RESULT RETURNED')
                    }
                    if (self.result){
                        console.log('RESULT ', self.result);
                    }
                })

                var tstream = through2.obj((obj,_,next) => {
                    next(null, {inputAudio: obj})
                })

                // pipe to dialogflow
                self.mic.getAudioStream()
                .pipe(tstream)
                .pipe(self.stream);

                self.mic.getAudioStream().on('startComplete', function() {
                    console.log("Got SIGNAL startComplete");
                })
                .on('data', function(data) {
                    //console.log("Received Input Stream: " + data.length);
                })
                .on('error', function(err) {
                    console.log("Error in Input Stream: " + err);
                })
                .on('silence', function() {
                    console.log("Got SIGNAL silence");

                    //self.unpipeTimeout= null;
                    tstream.unpipe(self.stream);
                    self.mic.getAudioStream().unpipe(tstream);
                    self.mic.stop();
                })
                .on('stopComplete', function() {
                    console.log("Got SIGNAL stopComplete");
                })
                .on('processExitComplete', function() {
                    console.log("Got SIGNAL processExitComplete");
                });

                self.mic.start();
            
        });
    }
}
 
async function run(){
    //wait for reproduce the welcome utterance
    /*await new Promise((resolve,reject)=>{
        setTimeout(()=>{
            resolve();
        },6500);
    });*/
    //run infinite
    var exit=false;
    var firstIteration = true;
    while (!exit){
        
        const gNew = new GoogleSpeech(initialStreamRequest);
        const resp = await gNew.startStream(firstIteration); 
        
        if (manualEnd || resp === 'end_conversation'){
            console.log('End conversation');
            exit=true; 
            send('{"filename":"albero_rotante.mp4"}')
        }
        else {
            firstIteration = ! firstIteration;
            console.log('Continue conversation');
            send('{"filename":"albero_pallina_ferma.mp4"}')
        }
    }
    isRunning = false; 
}

//run();