# Progetto Hollie - TIM Smart Spaces Hackathon

Progetto per la presentazione del team Softlab (team18) all'Hackaton TIM 2020

Dal punto di vista dei codici sorgenti, il software è organizzato all'interno di tre directory:

	- hollie_unity
	- hollie_nodejs
	- hollie_df_agent


La prima contiene il software in NodeJS che si occupa della comunicazione tra microfono/utente e DialogFlow. La seconda contiene il software Unity che gestisce le animazioni grafiche e l’ologramma. La terza contiene il codice dell’agent esportato da Google DialogFlow. All’esterno delle directory si trova lo script send_sms.php, responsabile dell’interazione tra Hollie e l’API di TIM per l’invio di SMS.

I requisiti software per il corretto funzionamento di Hollie sono:

	- Microsoft Windows10 Professional
	- node-v12.18.2-x64
	- sox-14.4.2-win32
	- Microsoft Visual C++ 2015-2019

Per poter avviare il server NodeJS è necessario utilizzare una chiave RSA legata al service account dell’agent DialogFlow. Una volta importato l’agent su DialogFlow, per ottenere la chiave sarà sufficiente cliccare sull’icona delle Impostazioni ---> Generale ---> Google Project ---> Service Account. All’apertura della pagina Account di servizio per il progetto Hollie cliccare su Crea Account di Servizio, inserire i dettagli richiesti e completare la procedura di creazione di un nuovo account. Al termine della procedura, cliccare sul nuovo account e, nella pagina Dettagli account di servizio, cliccare su Aggiungi Chiave.

Al termine dell’operazione verrà scaricata in locale la chiave necessaria al software di Hollie per dialogare con DialogFlow.

Per utilizzare la nuova chiave all’interno di Hollie, copiare il file con la chiave RSA all’interno della directory hollie_nodejs, quindi copiare il nome del file all’interno del file index_hollie.js in corrispondenza della riga 132, nel parametro keyFilename.

Procedere poi con la modifica del project ID riportato sulla riga 14 dello stesso file, utilizzando il Google Project ID che si trova su DialogFlow, nelle impostazioni generali dell’agent, all’interno del pannello Google Project.

Successivamente modificare il file \Hollie_src\send_sms.php nella sezione: $APIKEY="<INSERIRE_LA_API_KEY>"; inserendo l'API key associata al progetto Team18.

Al termine delle operazioni, per avviare il server NodeJS è sufficiente effettuare un doppio click sullo script batch startServer.bat all’interno della directory hollie_nodejs.
Per l’avvio del software dell’ologramma, effettuare un doppio click sul file batch StartHolo.bat all’interno della directory hollie_unity.
